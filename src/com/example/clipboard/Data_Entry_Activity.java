package com.example.clipboard;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;



import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Data_Entry_Activity extends ActionBarActivity implements OnClickListener,OnLongClickListener
{

	EditText name,uname,pwd,repwd,address;
	Button register,login;
	LayoutInflater factory;
	int sdk = android.os.Build.VERSION.SDK_INT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		name=(EditText)findViewById(R.id.edtname);
		uname=(EditText)findViewById(R.id.edtuname);
		
		pwd=(EditText)findViewById(R.id.edtpwd);
		pwd.setOnLongClickListener(this);
		
		repwd=(EditText)findViewById(R.id.edtrepwd);
		repwd.setOnLongClickListener(this);
		
		address=(EditText)findViewById(R.id.edtaddress);
		register=(Button)findViewById(R.id.btnregister);
		login=(Button)findViewById(R.id.btnlogin);
		register.setOnClickListener(this);
		login.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{	
		
		
		
	}

	@Override
	public boolean onLongClick(View v) 
	{
		if(v.getId()==R.id.edtpwd)
		{
			
			
			showdialog("Copy");
			
	
			
		}
		if(v.getId()==R.id.edtrepwd)
		{
			showdialog("Paste");
			
	
		}	
		
		return false;
	}
	
	
	
	
	public void showdialog(String clip)
    {
		
		if(clip.equals("Copy"))
		{
			factory = LayoutInflater.from(this);
	        View textEntryView = factory.inflate(R.layout.paste_dialog_box, null);
	        final AlertDialog.Builder alert = new AlertDialog.Builder(this);	        
	        final Button copy=(Button)textEntryView.findViewById(R.id.paste);
	        copy.setText("Copy");
			copy.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {					
					pwd.setTextIsSelectable(true);
					
					    if(pwd.getText().toString().length() != 0)
					    {
					    	
		                if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
		 
		                    android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		                    clipboard.setText(pwd.getText().toString());
		                    Toast.makeText(getApplicationContext(), "Text Copied to Clipboard", Toast.LENGTH_SHORT).show();
		 
		                } else {
		                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		                    android.content.ClipData clip = android.content.ClipData.newPlainText("Clip",pwd.getText().toString());
		                    Toast.makeText(getApplicationContext(), "Text Copied to Clipboard", Toast.LENGTH_SHORT).show();
		                    clipboard.setPrimaryClip(clip);
		                }}else{
		                    Toast.makeText(getApplicationContext(), "Nothing to Copy", Toast.LENGTH_SHORT).show();
		 
		                }					
				}
			});	        
	        alert.setView(copy);
			alert.show();   	
		}
		
		if(clip.equals("Paste"))
		{
			factory = LayoutInflater.from(this);
	        View textEntryView = factory.inflate(R.layout.paste_dialog_box, null);
	        final AlertDialog.Builder alert = new AlertDialog.Builder(this);	        
	        final Button paste=(Button)textEntryView.findViewById(R.id.paste);
	        paste.setOnClickListener(new OnClickListener() 
	        {
				
				@Override
				public void onClick(View v) {					
					 String pasteText;					 
			            // TODO Auto-generated method stub
			            if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB)
			            {
			            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			            pasteText = clipboard.getText().toString();
			            repwd.setText(pasteText);			 
			            }
			            else
			            {
			            
			            	 ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			                    if(clipboard.hasPrimaryClip()== true){
			                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
			                     pasteText = item.getText().toString();
			                     repwd.setText(pasteText);
			 
			                    }else{
			 
			                        Toast.makeText(getApplicationContext(), "Nothing to Paste", Toast.LENGTH_SHORT).show();
			 
			                    }
			            	
			            	
			            }					
				}
			});
	        alert.setView(paste);
			alert.show();   	
		}
		         
    }		
}
